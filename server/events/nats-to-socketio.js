const Promise = require('bluebird')
const { getNerveInstance } = require('../nats-ss')

exports.Bridge = class Bridge {
  constructor(client) {
    this.client = client
    this.subscriptions = {}
  }

  async subscribeToChannel(data) {
    const { channel, opts } = data
    try {
      const nerve = await getNerveInstance()
      const subscription = nerve.subscribe(channel, opts, msg => {
        const message = {
          sequence: msg.getSequence(),
          timestamp: msg.getTimestamp(),
          subject: msg.getSubject(),
          data: msg.getData()
        }
        this.client.emit(channel, message)
      })
      this.subscriptions[channel] = { nerve, subscription }
    } catch (err) {
      console.error(err)
    }
  }

  async unsubscribeFromChannel(channel) {
    const { subscription } = this.subscriptions[channel]
    return new Promise((resolve, reject) => {
      subscription.unsubscribe()
      subscription.on('unsubscribed', () => {
        resolve()
      })
    })
  }

  async unsubscribeAll() {
    const promises = this.subscriptions.map((sub, channel) => {
      this.unsubscribeFromChannel(channel)
    })
    return Promise.all(promises)
  }
}
